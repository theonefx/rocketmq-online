# <img src="http://rocketmq.apache.org/favicon.ico" width="5%"> RocketMQ 基础教程 -- 01 源码编译

Apache RocketMQ™ 是一个统一的消息引擎系统, 也是一个轻量级的数据处理平台.  

当你遇到以下类似问题而束手无策时，RocketMQ可以帮助你解决：

- 重试消息、死信消息、事物消息支持
- 消息链路追踪
- 消息过滤
- IPv6支持
- ACL支持
- 主、副本自动高可靠
- 全面的监控支持
- 管理平台原生支持
- Request-Reply模式支持

本教程会以如何利用源码编译并打包RocketMQ为例, 演示如何编译任意版本的RocketMQ. 

接下来将具体介绍整个编译过程以及步骤, 大家按部就班操作即可, 非常简单.



## 1. Git下载源码
```bash
git clone https://gitee.com/apache/rocketmq.git

cd rocketmq && git checkout rocketmq-all-4.8.0 && ls -l
```
源码结构如下:  
![源码结构](https://img.alicdn.com/imgextra/i4/O1CN01GM6SbR1HpeHemE4nN_!!6000000000807-2-tps-575-609.png "源码结构")
## 2. 设置maven为国内镜像仓库, 加快下载速度(这里忽略)

## 3. 进行maven编译, 大约4min多钟将完成编译
```bash
mvn -Prelease-all -DskipTests clean install -U
```
编译结果:  
![编译结果](https://img.alicdn.com/imgextra/i1/O1CN01DEXumg25kotcBgGvE_!!6000000007565-2-tps-752-582.png "编译结果")
## 4. 稍等几分钟内, 检查编译结果
```bash
cd ./distribution/target && ls -l
```
编译结果查看:  
![查看编译结果](https://img.alicdn.com/imgextra/i1/O1CN01bJxpjM1siuEexKugC_!!6000000005801-2-tps-715-261.png "编译结果检查")

## 教辅资料总结
- [RocketMQ源码](https://github.com/apache/rocketmq)
- [RocketMQ文档](http://rocketmq.apache.org/docs/quick-start/)
- RocketMQ钉钉群 / 微信加我拉群:  
  <img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/rmq-dd2.jpg" width="20%" /> <img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/wo-de-qr.jpeg" width="20%" />



### 更多RocketMQ原理解析, 详见[《RocketMQ分布式消息中间件：核心原理与最佳实践》](https://detail.tmall.com/item.htm?spm=a230r.1.14.15.1b778164FtH9y5&id=625997240150&ns=1&abbucket=7)
本书以RocketMQ 4.2.0和RocketMQ 4.3.0源码为基础，从RocketMQ的实际使用到RocketMQ的源码分析，再到RocketMQ企业落地实践方案，逐步讲解。使读者由浅入深地了解RocketMQ。  
本书在源码分析过程中，先讲整体流程，再按模块、步骤进行详细讲解，希望读者在阅读时能举一反三，能知其然且知其所以然。  
本书总共九章，分为五部分，第一部分讲解消息队列入门和RocketMQ生产、消费原理与最佳实践；第二部分从整体角度讲解RocketMQ架构；第三部分讲解RocketMQ各个组件的基本原理；第四部分深入RocketMQ，讲解如何阅读源代码、如何进行企业实践；第五部分是附录，包含Namesrv、Broker的核心参数配置说明和Exporter监控指标注释。  

希望读者在平时的工作中能熟悉、借鉴、参考RocketMQ的优秀设计理念，在技术能力上更进一步，在工作中更好地服务公司。  

<img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/bd-desc.png" />
<img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/zhuanjai%E8%AF%84%E8%AE%BA.png" />
