# <img src="http://rocketmq.apache.org/favicon.ico" width="5%"> RocketMQ分布式消息中间件：核心原理与最佳实践01-部署RocketMQ监控利器-Exporter

Apache RocketMQ™ 是一个统一的消息引擎系统, 也是一个轻量级的数据处理平台.  

当你遇到以下类似问题而束手无策时，RocketMQ可以帮助你解决：

- 重试消息、死信消息、事物消息支持
- 消息链路追踪
- 消息过滤
- IPv6支持
- ACL支持
- 主、副本自动高可靠
- 全面的监控支持
- 管理平台原生支持
- Request-Reply模式支持

RocketMQ Exporter是一个基于Prometheus的RocketMQ监控组件, 可以帮助大家监控、报警. 本教程会以如何利用源码编译、配置、部署RocketMQ Exporter, 并且查看监控指标

接下来将具体介绍整个操作过程以及步骤, 大家按部就班操作即可, 非常简单.



## 1. Git下载源码
```bash
git clone https://github.com/apache/rocketmq-exporter.git

cd rocketmq-exporter && ls -l
```
![代码结构](https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq-book/book01-01.png "代码结构")

## 2. 修改配置(本实验环境不用修改, 实际环境需要)
  ```console
  rocketmq:
    config:
      webTelemetryPath: /metrics
      rocketmqVersion: 4_8_0
      namesrvAddr: 127.0.0.1:9876;1.1.1.1:9876 ### 待修改
      enableCollect: true
      enableACL: false # if >=4.4.0  ### 待修改
      accessKey: # if >=4.4.0   ### 待修改
      secretKey: # if >=4.4.0   ### 待修改
  ```
  ```bash
  vim src/main/resources/application.yml
  ```
- namesrvAddr: Namesrv地址, 多个用分号隔开. 

- enableACL: Broker是否开启了ACL, 默认是false

- accessKey: Broker上ACL配置, 与secretKey成对配置, 一般用[Broker根目录/conf/plain_acl.yml](https://github.com/apache/rocketmq/blob/master/distribution/conf/plain_acl.yml)中配置角色为admin的账号. v

- secretKey: Broker上ACL配置, 与accessKey成对配置. 一般用[Broker根目录/conf/plain_acl.yml](https://github.com/apache/rocketmq/blob/master/distribution/conf/plain_acl.yml)中配置角色为admin的账号. 
![RocketMQ ACL配置Demo](https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq-book/broker-acl-admin.jpg "RocketMQ ACL配置Demo")
## 3. 编译源码
```bash
mvn clean package -DskipTests
```
![编译结果](https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq-book/book01-02.png "编译结果")

## 4. 初始化一个RocketMQ集群(1Namesrv+1Broker)
```bash
cd ~/ && sh install-rocketmq.sh && jps
```
install-rocketmq.sh脚本操作分为三步如下, 具体代码相见脚本注释.
- 下载RocketMQ编译结果
- 安装1个Namesrv服务, 端口为9876
- 使用本地Namesrv地址, 安装一个Broker服务

查看脚本内容:  
```bash
cd ~/ && cat install-rocketmq.sh
```

## 5. 启动Exporter
注意: rocketmq-exporter-0.0.2-SNAPSHOT.jar名字根据不同的版本会变化, 以实际编译结果为准.
```bash

cd ~/rocketmq-exporter && ls -l

nohup java -jar target/rocketmq-exporter-0.0.2-SNAPSHOT.jar --server.port=60000 &

```

```bash

tail -f ~/logs/exporterlogs/rocketmq-exporter.log

```


启动后, 会自动打出运行日志. 日志显示成功启动exporter后, 如果想退出, 使用: Ctrl + C退出日志查看.

PS:  
<i>1. 日志中可能存在not exist之类的报错, 是因为没有正式使用, 可以忽略.</i>

## 5. 监控指标查看
<tutorial-web-preview port="60000" path="/metrics">点我查看指标</tutorial-web-preview>  
或者  
```bash@terminal2
curl 127.0.0.1:60000/metrics
```


PS: 
- <i>1. 指标采集需要时间, 稍等一会curl</i>  
- <i>2. Exporter默认端口是5557, 60000是实验室转发端口</i>

![监控指标](https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq-book/book02-03.png "监控指标")

执行以上命令, 则可以看到采集的RocketMQ指标. 这么多指标, 每个指标代表什么含义呢?   
在《RocketMQ分布式消息中间件：核心原理与最佳实践》书中罗列了全部核心指标的含义, 欢迎大家参考.
![部分指标参考](https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq-book/book-02-04.png "部分指标参考")

### 教辅资料总结
- [RocketMQ Exporter源码](https://github.com/apache/rocketmq-exporter)
- [RocketMQ文档](http://rocketmq.apache.org/docs/quick-start/)
- RocketMQ钉钉群 / 微信加我拉群:  
  <img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/rmq-dd2.jpg" width="20%" /> <img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/wo-de-qr.jpeg" width="20%" />



### 更多RocketMQ原理解析, 详见[《RocketMQ分布式消息中间件：核心原理与最佳实践》](https://detail.tmall.com/item.htm?spm=a230r.1.14.15.1b778164FtH9y5&id=625997240150&ns=1&abbucket=7)
本书以RocketMQ 4.2.0和RocketMQ 4.3.0源码为基础，从RocketMQ的实际使用到RocketMQ的源码分析，再到RocketMQ企业落地实践方案，逐步讲解。使读者由浅入深地了解RocketMQ。  
本书在源码分析过程中，先讲整体流程，再按模块、步骤进行详细讲解，希望读者在阅读时能举一反三，能知其然且知其所以然。  
本书总共九章，分为五部分，第一部分讲解消息队列入门和RocketMQ生产、消费原理与最佳实践；第二部分从整体角度讲解RocketMQ架构；第三部分讲解RocketMQ各个组件的基本原理；第四部分深入RocketMQ，讲解如何阅读源代码、如何进行企业实践；第五部分是附录，包含Namesrv、Broker的核心参数配置说明和Exporter监控指标注释。  

希望读者在平时的工作中能熟悉、借鉴、参考RocketMQ的优秀设计理念，在技术能力上更进一步，在工作中更好地服务公司。  

<img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/bd-desc.png" />
<img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/zhuanjai%E8%AF%84%E8%AE%BA.png" />
