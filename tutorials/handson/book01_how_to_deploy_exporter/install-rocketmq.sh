#!bin/bash
RETVAL=0
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
shell_home=/home/shell
rocketmq_version=rocketmq-4.8.0
## 下载RocketMQ编译结果, 这里用最新版4.8.0
download_rocketmq()
{
    cd $shell_home
    rocketmq_home=$shell_home/$rocketmq_version
    wget https://handson.oss-cn-shanghai.aliyuncs.com/$rocketmq_version.tar.gz --no-check-certificate
    tar -zxvf $rocketmq_version.tar.gz && cd $rocketmq_version && ls -l
    echo '-------RocketMQ download success-------' `date '+%Y-%m-%d %H:%M:%S'`
}

## 安装Namesrv服务
install_namesrv()
{
    rocketmq_home=$shell_home/$rocketmq_version

    cd $rocketmq_home/conf/ && touch kvConfig.properties
    wget https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/namesrv.conf --no-check-certificate  ## 下载Namesrv配置

    cd $rocketmq_home
    sed -i 's/Xms4g -Xmx4g -Xmn2g/Xms300m -Xmx300m -Xmn200m/g' ./bin/runserver.sh ## 由于实验环境配置不高, 需要修改jvm参数

    nohup ./bin/mqnamesrv -c ./conf/namesrv.conf > /dev/null 2>&1 &
    echo '-------Namesrv boot success-------' `date '+%Y-%m-%d %H:%M:%S'`
}

## 安装Broker服务
install_broker()
{
    rocketmq_home=$shell_home/$rocketmq_version

    cd $rocketmq_home/conf
    wget https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/async_broker.conf --no-check-certificate ## 下载Broker配置

    mkdir -p /data/rocketmq/commitlog
    mkdir -p /data/rocketmq/consumequeue

    cd $rocketmq_home
    sed -i 's/Xms8g -Xmx8g -Xmn4g/Xms1g -Xmx1g -Xmn500m/g' ./bin/runbroker.sh ## 由于实验环境配置不高, 需要修改jvm参数

    nohup sh ./bin/mqbroker -c ./conf/async_broker.conf > /dev/null 2>&1 & ## 启动Broker服务
    echo '-------Broker boot success-------' `date '+%Y-%m-%d %H:%M:%S'`
}

download_rocketmq

install_namesrv

install_broker

echo '已经成功安装一个简单的RocketMQ集群(1个Namesrv + 1个Broker), 脚本如何执行的, 可以打开脚本看注释.'

exit $RETVAL