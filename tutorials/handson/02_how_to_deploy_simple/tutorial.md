# <img src="http://rocketmq.apache.org/favicon.ico" width="5%"> RocketMQ 基础教程 -- 02 如何部署一个简单集群

Apache RocketMQ™ 是一个统一的消息引擎系统, 也是一个轻量级的数据处理平台.  

当你遇到以下类似问题而束手无策时，RocketMQ可以帮助你解决：

- 重试消息、死信消息、事物消息支持
- 消息链路追踪
- 消息过滤
- IPv6支持
- ACL支持
- 主、副本自动高可靠
- 全面的监控支持
- 管理平台原生支持
- Request-Reply模式支持

本教程会以如何利用[RocketMQ 基础教程 -- 01 源码编译](https://start.aliyun.com/handson/rocketmq-online/01_how_to_build_from_source)实验结果, 部署一个1Namesrv+1Broker的集群.

接下来将具体介绍整个操作过程以及步骤, 大家按部就班操作即可, 非常简单.



## 1. 下载、解压缩RocketMQ4.8.0版本的编译结果
```bash
wget https://handson.oss-cn-shanghai.aliyuncs.com/rocketmq-4.8.0.tar.gz

tar -zxvf rocketmq-4.8.0.tar.gz

cd rocketmq-4.8.0 && ls -l
```
解压包目录结构如下:  
![解压包后的目录结构](https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/02-01.jpg "解压包后的目录结构")
## 2. 配置、启动Namesrv
```bash


```

## 3. 配置启动Broker

## 4. 验证1: 查看集群信息

## 5. 验证2: 发送、消费消息验证





## 教辅资料总结
- [RocketMQ源码](https://github.com/apache/rocketmq)
- [RocketMQ文档](http://rocketmq.apache.org/docs/quick-start/)
- RocketMQ钉钉群 / 微信加我拉群:  
  <img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/rmq-dd2.jpg" width="20%" /> <img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/wo-de-qr.jpeg" width="20%" />



### 更多RocketMQ原理解析, 详见[《RocketMQ分布式消息中间件：核心原理与最佳实践》](https://detail.tmall.com/item.htm?spm=a230r.1.14.15.1b778164FtH9y5&id=625997240150&ns=1&abbucket=7)
本书以RocketMQ 4.2.0和RocketMQ 4.3.0源码为基础，从RocketMQ的实际使用到RocketMQ的源码分析，再到RocketMQ企业落地实践方案，逐步讲解。使读者由浅入深地了解RocketMQ。  
本书在源码分析过程中，先讲整体流程，再按模块、步骤进行详细讲解，希望读者在阅读时能举一反三，能知其然且知其所以然。  
本书总共九章，分为五部分，第一部分讲解消息队列入门和RocketMQ生产、消费原理与最佳实践；第二部分从整体角度讲解RocketMQ架构；第三部分讲解RocketMQ各个组件的基本原理；第四部分深入RocketMQ，讲解如何阅读源代码、如何进行企业实践；第五部分是附录，包含Namesrv、Broker的核心参数配置说明和Exporter监控指标注释。  

希望读者在平时的工作中能熟悉、借鉴、参考RocketMQ的优秀设计理念，在技术能力上更进一步，在工作中更好地服务公司。  

<img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/bd-desc.png" />
<img src="https://code.aliyun.com/handsonlabs_rocketmq/lab_images/raw/master/rmq/zhuanjai%E8%AF%84%E8%AE%BA.png" />
